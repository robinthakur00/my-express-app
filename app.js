const express = require('express');
const app = express();
const port = 3000;

// Simple route handling GET requests to the root URL
app.get('/', (req, res) => {
  res.send('Hello, this is your Express app!');
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
